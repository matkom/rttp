use clap::Parser;
use std::{
    io::{BufRead, BufReader, Write},
    net::{SocketAddr, TcpListener, TcpStream},
    path::Path,
    fs,
};

#[derive(Parser, Debug)]
#[command()]
pub struct Args {
    served_path: String,

    #[arg(long, default_value = "127.0.0.1")]
    ip: String,

    #[arg(long, default_value = "6969")]
    port: String,
}

// TODO Add multithreading for fun
fn main() {
    let args = Args::parse();
    let served_path = Path::new(&args.served_path);
    let addr = format!("{}:{}", &args.ip, &args.port);

    println!("[INFO] Listening on {}", addr);
    match TcpListener::bind(addr) {
        Ok(listener) => loop {
            match listener.accept() {
                Ok((tcp_stream, addr)) => handle_request(tcp_stream, addr, &served_path),
                Err(err) => eprintln!("[ERROR] Could not accept connection. Reason: {}", err),
            }
        },
        Err(err) => eprintln!("[ERROR] Could not open TCP socket. Reason: {}", err),
    }
}

fn handle_request(mut tcp_stream: TcpStream, addr: SocketAddr, served_path: &Path) {
    println!("[INFO] Accepted connection from: {}", addr);

    let reader = BufReader::new(&mut tcp_stream);
    let mut request = Vec::new();

    for line_result in reader.lines() {
        match line_result {
            Ok(line) if line.is_empty() => break,
            Ok(line) => request.push(line),
            Err(err) => eprintln!(
                "[ERROR] Could not read data from TCP stream. Reason: {}",
                err
            ),
        }
    }

    println!("[INFO] Request content:\n{:#?}", request);

    if let Some(requested_resource) = parse_request(&request) {
        serve_resource(served_path, requested_resource, &mut tcp_stream);
    }
}

fn parse_request(request: &Vec<String>) -> Option<&str> {
    if request.len() < 1 {
        eprintln!("[ERROR] Request does not contain HTTP request line");
        return None;
    }

    let request_line_tokens: Vec<_> = request[0].split_whitespace().collect();

    if request_line_tokens.len() < 3 {
        eprintln!("[ERROR] Invalid HTTP request");
        return None;
    }

    if request_line_tokens[0] != "GET" {
        eprintln!("[ERROR] Server supports only GET requests");
        return None;
    }

    if request_line_tokens[2] != "HTTP/1.1" {
        eprint!("[ERROR] Server supports only HTTP/1.1");
        return None;
    }

    Some(request_line_tokens[1])
}

fn serve_resource(served_path: &Path, requested_resource: &str, tcp_stream: &mut TcpStream) {
    let normalized_requested_resource = requested_resource.strip_prefix("/").unwrap_or_default();

    if served_path.is_file() {
        let filename = served_path
            .file_name()
            .expect("[ERROR] Could not read served file name");
        if filename == normalized_requested_resource {
            send_file(tcp_stream, served_path);
        } else {
            resource_not_found(tcp_stream);
        }
    } else if served_path.is_dir() {
        let filepath = served_path.join(normalized_requested_resource);
        if filepath.exists() {
            send_file(tcp_stream, &filepath)
        } else {
            resource_not_found(tcp_stream);
        }
    }
}

fn send_file(tcp_stream: &mut TcpStream, filepath: &Path) {
    match fs::read_to_string(filepath) {
        Ok(file_content) => {
            let response = format!(
                "HTTP/1.1 200 OK\nContent-Length:{}\n\n{}",
                file_content.len(),
                file_content
            );
            send_response(tcp_stream, &response);
        }
        Err(err) => {
            eprintln!("[ERROR] {}", err);
            let response_msg = "Sorry we had trouble reading requested resource :(";
            let response = format!(
                "HTTP/1.1 500 Internal Server Error\nContent-Length:{}\n\n{}",
                response_msg.len(),
                response_msg
            );
            send_response(tcp_stream, &response);
        }
    }
}

fn resource_not_found(tcp_stream: &mut TcpStream) {
    let err_msg = "Requested resource was not found";
    let response = format!(
        "HTTP/1.1 404 Not Found\nContent-Length:{}\n\n{}",
        err_msg.len(),
        err_msg
    );
    send_response(tcp_stream, &response);
}

fn send_response(tcp_stream: &mut TcpStream, response: &str) {
    if let Err(err) = tcp_stream.write_all(response.as_bytes()) {
        eprintln!("[ERROR] Could not send response. Reason: {}", err);
    }
}
